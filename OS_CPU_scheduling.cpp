#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <list>

using namespace std;

class process;
void SJF_Preemp_schedule(list<process>);
void FCFS_schedule(list<process>);
void round_robin_schedule(list<process>, int);
list<process>::iterator Lowest_burst_time(list<process>);
bool Is_success(list<process>);
double average_waiting_time(list<process>);
double average_turnaround_time(list<process>);
double average_response_time(list<process>);

class process {
public:
	process(int Pno, int a) {
		process_no = Pno;
		if (a == 1) {
			burst_time = rand() % 25 + 1;
			arrival_time = rand() % 50 + 1;
		}
		else {
			burst_time = rand() % 25 + 26;
			arrival_time = rand() % 50 + 1;
		}
		wait_time = 0;
		turnaround_time = 0;
		response_time = 0;
		complete_status = false;
		response_status = false;
	}
	int get_wt() { return wait_time; }
	int get_burst() { return burst_time; }
	int get_arr() { return arrival_time; }
	int get_Pno() { return process_no; }
	int get_tt() { return turnaround_time; }
	int get_rt() { return response_time; }
	bool get_status() { return complete_status; }
	bool get_restatus() { return response_status; }
	void set_wt(int wt) { wait_time = wt; }
	void set_burst(int burst) { burst_time = burst; }
	void set_arr(int arr) { arrival_time = arr; }
	void set_tt(int tt) { turnaround_time = tt; }
	void set_rt(int rt) { response_time = rt; }
	void set_status(bool status) { complete_status = status; }
	void set_restatus(bool status) { response_status = status; }
	friend bool operator <(process a, process b) {
		if (a.arrival_time < b.arrival_time) {
			return true;
		}
		else {
			return false;
		}
	}
private:
	int process_no;
	int burst_time;
	int arrival_time;
	int wait_time;
	int turnaround_time;
	int response_time;
	bool complete_status;
	bool response_status;
};

int main() {
	list<process> processes;
	srand(1);
	for (int i = 0; i < 30; i++) {
		process new_process(i + 1, 2);
		processes.push_back(new_process);
	}
	for (int i = 30; i < 40; i++) {
		process new_process(i + 1, 1);
		processes.push_back(new_process);
	}
	processes.sort();
	FCFS_schedule(processes);
	SJF_Preemp_schedule(processes);
	round_robin_schedule(processes, 10);

	return 0;
}

void SJF_Preemp_schedule(list<process> processes) {
	list<process> wait_process;
	double current_time = 0;
	do {
		for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
			if (it->get_arr() == current_time) {
				wait_process.push_back(*it);
			}
		}
		if (wait_process.size()) {
			list<process>::iterator lowest_burst_time = Lowest_burst_time(wait_process);
			int Pno_lowest_burst_time = lowest_burst_time->get_Pno();
			list<process>::iterator it = wait_process.begin();
			while (it != wait_process.end()) {
				int Pno = it->get_Pno();
				if (it->get_Pno() == Pno_lowest_burst_time) {
					it->set_burst(it->get_burst() - 1);
					if (!it->get_restatus()) {
						for (list<process>::iterator itt = processes.begin(); itt != processes.end(); itt++) {
							if (itt->get_Pno() == Pno) {
								itt->set_rt((current_time - it->get_arr()));
								itt->set_restatus(true);
								it->set_restatus(true);
							}
						}
					}
					if (it->get_burst() == 0) {
						it = wait_process.erase(it);
						for (list<process>::iterator itt = processes.begin(); itt != processes.end(); itt++) {
							if (itt->get_Pno() == Pno) {
								itt->set_status(true);
							}
						}
						continue;
					}
				}
				else {
					for (list<process>::iterator itt = processes.begin(); itt != processes.end(); itt++) {
						if (itt->get_Pno() == Pno) {
							itt->set_wt(itt->get_wt() + 1);
						}
					}
				}
				it++;
			}
		}
		current_time++;
	} while (!Is_success(processes));
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		it->set_tt((it->get_wt() + it->get_burst()));
	}
	int process_no = 1;
	cout << "--------------------------------------------------------------------------------------------" << endl;
	cout << "Preemptive Shortest Job First Scheduling" << endl;
	cout << "--------------------------------------------------------------------------------------------" << endl;
	cout << setw(10) << "Process NO.|" << setw(15) << "arrival time|" << setw(15) << "burst time|" << setw(15) << "wait time|" << setw(20) << "turnaround time|" << setw(15) << "response time|" << endl;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		cout << setw(11) << it->get_Pno() << "|" << setw(14) << it->get_arr() << "|" << setw(14) << it->get_burst() << "|" << setw(14) << it->get_wt() << "|" << setw(19) << it->get_tt() << "|" << setw(14) << it->get_rt() << "|" << endl;
	}
	cout << "Average waiting time = " << average_waiting_time(processes) << endl;
	cout << "Average turnaround time = " << average_turnaround_time(processes) << endl;
	cout << "Average response time = " << average_response_time(processes) << endl;
}

void FCFS_schedule(list<process> processes) {
	double current_time = 0;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		int wait_time = 0;
		int turnaround_time = 0;
		int response_time = 0;
		double arrival_time = (*it).get_arr();
		double burst_time = (*it).get_burst();
		if (arrival_time > current_time) {
			current_time += (arrival_time + burst_time);
			turnaround_time = burst_time + wait_time;
			it->set_wt(wait_time);
			it->set_tt(turnaround_time);
			it->set_rt(response_time);
		}
		else {
			wait_time = current_time - arrival_time;
			turnaround_time = burst_time + wait_time;
			response_time = wait_time;
			current_time += burst_time;
			it->set_wt(wait_time);
			it->set_tt(turnaround_time);
			it->set_rt(response_time);
		}
	}
	cout << "--------------------------------------------------------------------------------------------" << endl;
	cout << "First Come First Served Algorithms (FCFS)" << endl;
	cout << "--------------------------------------------------------------------------------------------" << endl;
	cout << setw(10) << "Process NO.|" << setw(15) << "arrival time|" << setw(15) << "burst time|" << setw(15) << "wait time|" << setw(20) << "turnaround time|" << setw(15) << "response time|" << endl;
	int process_no = 1;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		cout << setw(11) << process_no << "|" << setw(14) << it->get_arr() << "|" << setw(14) << it->get_burst() << "|" << setw(14) << it->get_wt() << "|" << setw(19) << it->get_tt() << "|" << setw(14) << it->get_rt() << "|" << endl;
		process_no++;
	}
	cout << "Average waiting time = " << average_waiting_time(processes) << endl;
	cout << "Average turnaround time = " << average_turnaround_time(processes) << endl;
	cout << "Average response time = " << average_response_time(processes) << endl;
}

void round_robin_schedule(list<process> processes, int qt) {
	list<process> backup_process;
	int count = 0;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		backup_process.push_back(*it);
	}
	double current_time = 0;
	list<process>::iterator it = backup_process.begin();
	list<process>::iterator itt = processes.begin();
	while (!Is_success(backup_process)) {
		if (it->get_burst() != 0) {
			int burst_time = it->get_burst();
			int wait_time = it->get_wt();
			int arrival_time = it->get_arr();
			if (burst_time < qt) {
				wait_time += (current_time - arrival_time);
				it->set_wt(wait_time);
				it->set_burst(0);
				if (!it->get_restatus()) {
					it->set_rt(current_time);
					it->set_restatus(true);
				}
				current_time += burst_time;
				it->set_status(true);
			}
			else {
				wait_time += (current_time - arrival_time);
				it->set_burst(burst_time - qt);
				it->set_wt(wait_time);
				it->set_arr(current_time + qt);
				if (!it->get_restatus()) {
					it->set_rt(current_time);
					it->set_restatus(true);
				}
				if (it->get_burst() == 0) {
					it->set_status(true);
				}
				current_time += qt;
			}
		}
		if (it == backup_process.end()) {
			it = backup_process.begin();
			itt = processes.begin();
		}
		else {
			it++;
			itt++;
		}
	}
	list<process>::iterator itp = processes.begin();
	for (list<process>::iterator itb = backup_process.begin(); itb != backup_process.end(); itb++) {
		itb->set_tt(itb->get_wt() + itp->get_burst());
		itp->set_wt(itb->get_wt());
		itp->set_tt(itb->get_tt());
		itp->set_rt(itb->get_rt());
		itp++;
	}
	int process_no = 1;
	itp = processes.begin();
	cout << "-----------------------------------------------------------------------------" << endl;
	cout << "Round Robin scheduling" << endl;
	cout << "-----------------------------------------------------------------------------" << endl;
	cout << setw(10) << "Process NO.|" << setw(15) << "burst time|" << setw(15) << "wait time|" << setw(20) << "turnaround time|" << setw(15) << "respomse time|" << endl;
	for (itp; itp != processes.end(); itp++) {
		cout << setw(11) << process_no++ << "|" << setw(14) << itp->get_burst() << "|" << setw(14) << itp->get_wt() << "|" << setw(19) << itp->get_tt() << "|" << setw(14) << itp->get_rt() << "|" << endl;
	}
	cout << "Average waiting time = " << average_waiting_time(processes) << endl;
	cout << "Average turnaround time = " << average_turnaround_time(processes) << endl;
	cout << "Average response time = " << average_response_time(processes) << endl;
}

list<process>::iterator Lowest_burst_time(list<process> processes) {
	list<process>::iterator lowest_burst_time = processes.begin();
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		if (it->get_burst() < lowest_burst_time->get_burst() && it->get_burst() > 0) {
			lowest_burst_time = it;
		}
	}
	return lowest_burst_time;
}

bool Is_success(list<process> processes) {
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		if (!it->get_status()) {
			return false;
		}
	}
	return true;
}

double average_waiting_time(list<process> processes) {
	double total_wt = 0;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		total_wt += it->get_wt();
	}
	return total_wt / processes.size();
}


double average_turnaround_time(list<process> processes) {
	double total_tt = 0;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		total_tt += it->get_tt();
	}
	return total_tt / processes.size();
}

double average_response_time(list<process> processes) {
	double total_rt = 0;
	for (list<process>::iterator it = processes.begin(); it != processes.end(); it++) {
		total_rt += it->get_rt();
	}
	return total_rt / processes.size();
}
